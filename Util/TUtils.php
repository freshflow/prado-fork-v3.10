<?php
/** just utils *
 */ 
function str_replace_nth($search, $replace, $subject, $nth)
{
    $found = preg_match_all('/'.preg_quote($search).'/', $subject, $matches, PREG_OFFSET_CAPTURE);
    if (false !== $found && $found > $nth) {
        return substr_replace($subject, $replace, $matches[0][$nth][1], strlen($search));
    }
    return $subject;
}

function filter_token_fix($sql)
{
    return str_replace(['%filter%','::%filter%'],':%filter%',$sql);
}

?>