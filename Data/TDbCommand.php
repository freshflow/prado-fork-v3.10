<?php
/**
 * TDbCommand class file
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @link http://www.pradosoft.com/
 * @copyright Copyright &copy; 2005-2010 PradoSoft
 * @license http://www.pradosoft.com/license/
 * @version $Id: TDbCommand.php 2920 2011-05-21 19:29:39Z ctrlaltca@gmail.com $
 * @package System.Data
 */

/**
 * TDbCommand class.
 *
 * TDbCommand represents an SQL statement to execute against a database.
 * It is usually created by calling {@link TDbConnection::createCommand}.
 * The SQL statement to be executed may be set via {@link setText Text}.
 *
 * To execute a non-query SQL (such as insert, delete, update), call
 * {@link execute}. To execute an SQL statement that returns result data set
 * (such as select), use {@link query} or its convenient versions {@link queryRow}
 * and {@link queryScalar}.
 *
 * If an SQL statement returns results (such as a SELECT SQL), the results
 * can be accessed via the returned {@link TDbDataReader}.
 *
 * TDbCommand supports SQL statment preparation and parameter binding.
 * Call {@link bindParameter} to bind a PHP variable to a parameter in SQL.
 * Call {@link bindValue} to bind a value to an SQL parameter.
 * When binding a parameter, the SQL statement is automatically prepared.
 * You may also call {@link prepare} to explicitly prepare an SQL statement.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @version $Id: TDbCommand.php 2920 2011-05-21 19:29:39Z ctrlaltca@gmail.com $
 * @package System.Data
 * @since 3.0
 */
class TDbCommand extends TComponent
{
	private $_connection;
	private $_text='';
    /**
     * @var PDOStatement
     */
	private $_statement=null;
	public $_unescapedBindings=[];
	public $_valueBindings=[];
	public $_paramBindings=[];
	private $_t;
	private $_dn='mysql';

	/**
	 * Constructor.
	 * @param TDbConnection $connection the database connection
	 * @param string $text the SQL statement to be executed
	 */
	public function __construct(TDbConnection $connection,$text)
	{
		$this->_connection=$connection;
		$this->_dn = strtolower($this->_connection->getDriverName());
        if ($this->_dn === TDbConnection::DRIVER_OCI) {
            $this->_t = FSqlTransform::factory($this);
        }
        $this->setText($text);
	}

	public function getTransform() {
		return $this->_t;
	}
	/**
	 * Set the statement to null when serializing.
	 */
	public function __sleep()
	{
		$this->_statement=null;
		return array_keys(get_object_vars($this));
	}

	/**
	 * destruct
	 */
	public function __destruct()
	{
		//if ($this->_statement) $this->_statement->closeCursor();
		$this->_statement=null;		
	}
	
	
	public function getUnescapedBindings() {
		return $this->_unescapedBindings;
	}

	public function getParamBindings() {
		return $this->_paramBindings;
	}

	public function getValueBindings() {
		return $this->_valueBindings;
	}

	/**
	 * @return string the SQL statement to be executed
	 */
	public function getText()
	{
		return $this->_text;
	}

	/**
	 * Specifies the SQL statement to be executed.
	 * Any previous execution will be terminated or cancel.
	 * @param string $value the SQL statement to be executed
	 */
	public function setText($value)
	{
		$this->_text=$value;
		$this->cancel();
	}

	/**
	 * @return TDbConnection the connection associated with this command
	 */
	public function getConnection()
	{
		return $this->_connection;
	}

	/**
	 * @return PDOStatement the underlying PDOStatement for this command
	 * It could be null if the statement is not prepared yet.
	 */
	public function getPdoStatement()
	{
		return $this->_statement;
	}

	/**
	 * Prepares the SQL statement to be executed.
	 * For complex SQL statement that is to be executed multiple times,
	 * this may improve performance.
	 * For SQL statement with binding parameters, this method is invoked
	 * automatically.
	 */
	public function prepare() { }
	public function prepareNow()
	{
		if($this->_statement==null)
		{
			try
			{
                if ($this->_dn === TDbConnection::DRIVER_OCI) {
                    $this->_t->doTransform();
                }
				$this->_statement=$this->getConnection()->getPdoInstance()->prepare($this->getText());				
			}
			catch(Exception $e)
			{
                error_log('Error in SQL: ' . $this->getText());
                if (Prado::isDebugMode()) {
                    error_log($e->getMessage());
                    error_log($e->getTraceAsString());
                }
				throw new TDbException('dbcommand_prepare_failed',$e->getMessage(),$this->getText());
			}
		}
	}

	/**
	 * Cancels the execution of the SQL statement.
	 */
	public function cancel()
	{
		$this->_statement=null;
	}

    /**
     * Binds a parameter to the SQL statement to be executed.
     * @param mixed $name Parameter identifier. For a prepared statement
     * using named placeholders, this will be a parameter name of
     * the form :name. For a prepared statement using question mark
     * placeholders, this will be the 1-indexed position of the parameter.
     * @param mixed $value Name of the PHP variable to bind to the SQL statement parameter
     * @param int $dataType SQL data type of the parameter
     * @param int $length length of the data type
     * @throws TSystemException
     * @see http://www.php.net/manual/en/function.PDOStatement-bindParam.php
     */
	public function bindParameter($name, &$value, $dataType=null, $length=null)
	{
		if ($this->_dn == TDbConnection::DRIVER_OCI) {
			if ($value instanceof FDbExpression) {
				if (is_int($name))
				{
					throw new TSystemException('cannot_bind_integer_params_with_unescaped',$name,$value);
				}
				$this->_unescapedBindings[$name] = (object) ['name'=>$name, 'param'=>(string) $value,'dataType'=>$dataType,'length'=>$length];
			}
			else {
				$this->_paramBindings[$name] = (object) ['name'=>$name, 'param'=> &$value,'dataType'=>$dataType,'length'=>$length];
			}
			return;
		}

		if ($value instanceof FDbExpression) {
			if ($this->_statement) {
				throw new TSystemException('cannot_bind_prepared_with_unescaped');
			}
			if (is_int($name))
			{
				throw new TSystemException('cannot_bind_integer_params_with_unescaped',$name,$value);
			}
			$this->_text = str_replace($name,$value,$this->_text);
			return;
		}
		
		$this->prepareNow();
		if($dataType===null)
			$this->_statement->bindParam($name,$value);
		else if($length===null)
			$this->_statement->bindParam($name,$value,$dataType);
		else
			$this->_statement->bindParam($name,$value,$dataType,$length);
	}
	
	private function bindParametersNow()
	{
		//if ($this->_dn != TDbConnection::DRIVER_OCI) return;
		$this->prepareNow();
		foreach ($this->_paramBindings as $name => $obj)
		{
            if (is_array($obj->param)) {
                $obj->param = &$obj->param[0]->{$obj->param[1]};
            }
            if (!property_exists($obj, 'dataType') || $obj->dataType === null) {
                $this->_statement->bindParam($obj->name, $obj->param);
            } else if ($obj->length === null) {
                $this->_statement->bindParam($obj->name, $obj->param, $obj->dataType);
            } else {
                $this->_statement->bindParam($obj->name, $obj->param, $obj->dataType, $obj->length);
            }
		}
		$this->_paramBindings = [];
	}

	public function bindUnescapedNow()
	{
		//if ($this->_dn != TDbConnection::DRIVER_OCI) return;

		if ($this->_statement) {
			return; //throw new TSystemException('cannot_bind_prepared_with_unescaped');
		}
		$from = []; $to = [];
		foreach($this->_unescapedBindings as $k => $v) {
			$from[] = $v->name;
			$to[] = $v->param;
		}
		
		$this->_text = str_ireplace($from,$to,$this->_text);
	}

	public function bindValues($params)
	{
		foreach($params as $p => $v)
		{
			$this->bindValue($p,$v);
		}
	}

    /**
     * Binds a value to a parameter.
     * @param mixed $name Parameter identifier. For a prepared statement
     * using named placeholders, this will be a parameter name of
     * the form :name. For a prepared statement using question mark
     * placeholders, this will be the 1-indexed position of the parameter.
     * @param mixed $value The value to bind to the parameter
     * @param int $dataType SQL data type of the parameter
     * @throws TSystemException
     * @see http://www.php.net/manual/en/function.PDOStatement-bindValue.php
     */
	public function bindValue($name, $value, $dataType=null)
	{
		if ($this->_dn == TDbConnection::DRIVER_OCI) {
			if ($value instanceof FDbExpression) {
				$this->_unescapedBindings[$name] = (object) ['name'=>$name, 'param'=>(string) $value,'dataType'=>$dataType];
			}
			else {
				$this->_valueBindings[$name] = (object) ['name'=>$name, 'param'=>$value,'dataType'=>$dataType];
			}
			return;
		}

        if ($value instanceof FDbExpression) {
			/*if ($this->_statement) {
				throw new TSystemException('cannot_bind_prepared_with_unescaped');
			}*/
			$this->_text = (is_int($name)) ? str_replace_nth('?', $value, $this->_text, $name) : str_replace($name,$value,$this->_text);
			return;
		}

		$this->prepareNow();
		if($dataType===null)
			$this->_statement->bindValue($name,$value);			
		else
			$this->_statement->bindValue($name,$value,$dataType);
	}
	
	private function bindValuesNow()
	{
		//if ($this->_dn != TDbConnection::DRIVER_OCI) return;
		$this->prepareNow();
		//var_dump($this->_valueBindings);
		foreach ($this->_valueBindings as $name => $obj)
		{
			if($obj->dataType===null)
				$this->_statement->bindValue($obj->name,$obj->param);
			else
				$this->_statement->bindValue($obj->name,$obj->param,$obj->dataType);
		}
		$this->_valueBindings = [];
	}

	/**
	 * Executes the SQL statement.
	 * This method is meant only for executing non-query SQL statement.
	 * No result set will be returned.
	 * @return integer number of rows affected by the execution.
	 * @throws TDbException execution failed
	 */
	public function execute()
	{
		try
		{
			// Do not trace because it will remain even in
			// Performance mode or when pradolite.php is used
			// Prado::trace('Execute Command: '.$this->getDebugStatementText(), 'System.Data');
            $this->doExecute();
            return $this->_statement->rowCount();
		}
		catch(Exception $e)
		{
            error_log('Error in SQL: ' . ($this->_statement instanceof PDOStatement ? $this->_statement->queryString : $this->getText()));
            if (Prado::isDebugMode()) {
                error_log($e->getMessage());
                error_log($e->getTraceAsString());
            }
			throw new TDbException('dbcommand_execute_failed',$e->getMessage(),$this->getDebugStatementText());
		}
	}

	/**
	 * @return String prepared SQL text for debugging purposes.
	 */
	public function getDebugStatementText()
	{
		if(Prado::getApplication()->getMode() === TApplicationMode::Debug)
			return $this->_statement instanceof PDOStatement ?
				$this->_statement->queryString
				: $this->getText();
		return '';
	}

	
	public function doExecute()
	{
		if($this->_statement==null) {
		    if ($this->_dn === TDbConnection::DRIVER_OCI) {
                $this->_t->doTransform();
                $this->bindUnescapedNow();
            }

            if (strpos($this->_text, "'%filter%'") !== false) {
                $this->_text = str_replace("'%filter%'", 'NULL', $this->_text);
            }
            $this->_statement=$this->getConnection()->getPdoInstance()->prepare($this->getText());
		}

        if ($user = Prado::getApp()->getUser()) {
            if (strpos($this->_text, ':USER_UID') !== false) {
                $this->_paramBindings[':USER_UID'] = (object)['name' => ':USER_UID', 'param' => $user->getUid()];
            }
            if (strpos($this->_text, ':USER_UPNAME') !== false) {
                $this->_paramBindings[':USER_UPNAME'] = (object)['name' => ':USER_UPNAME', 'param' => $user->getUserRecord()->getDF()];
            }
            if (strpos($this->_text, ':USER_MAXLEVEL') !== false) {
                $this->_paramBindings[':USER_MAXLEVEL'] = (object)['name' => ':USER_MAXLEVEL', 'param' => $user->getMaxLevel()];
            }
            if (strpos($this->_text, ':DEFAULT_RELDEF_SET') !== false) {
                $maxDefSysTemplateUid = Prado::getApp()->getParameterValue('definitionMaxRelDefSet',300);
                $defaultValue = Prado::getApp()->getParameterValue('definitionForceUpname',false) || $user->getUserRecord()->getDF() <= $maxDefSysTemplateUid  ? 0 : -1;
                $defaultRelDefSet = Prado::getApp()->getParameterValue('definitionRelDefSet',$defaultValue);
                $this->_paramBindings[':DEFAULT_RELDEF_SET'] = (object)['name' => ':DEFAULT_RELDEF_SET', 'param' => $defaultRelDefSet ];
            }
        }

        //echo '<pre>';
		//var_dump($this->_paramBindings,$this->_valueBindings, $this->_unescapedBindings, $this->_text);

		$this->bindParametersNow();
		$this->bindValuesNow();

		return $this->_statement->execute();
	}
	/**
	 * Executes the SQL statement and returns query result.
	 * This method is for executing an SQL query that returns result set.
	 * @return TDbDataReader the reader object for fetching the query result
	 * @throws TDbException execution failed
	 */
	public function query()
    {
		try
		{
			// Prado::trace('Query: '.$this->getDebugStatementText(), 'System.Data');
            $this->doExecute();
			return new TDbDataReader($this);
		}
		catch(Exception $e)
		{
            error_log('Error in SQL: ' . ($this->_statement instanceof PDOStatement ? $this->_statement->queryString : $this->getText()));
            if (Prado::isDebugMode()) {
                error_log($e->getMessage());
                error_log($e->getTraceAsString());
            }
			throw new TDbException('dbcommand_query_failed',$e->getMessage(),$this->getDebugStatementText());
		}
	}

	/**
	 * Executes the SQL statement and returns the first row of the result.
	 * This is a convenient method of {@link query} when only the first row of data is needed.
	 * @param boolean $fetchAssociative whether the row should be returned as an associated array with
	 * column names as the keys or the array keys are column indexes (0-based).
	 * @return array the first row of the query result, false if no result.
	 * @throws TDbException execution failed
	 */
	public function queryRow($fetchAssociative=true)
	{
		try
		{
			//Prado::trace('Query Row: '.$this->getDebugStatementText(), 'System.Data');
			$this->doExecute();
			$result=$this->_statement->fetch($fetchAssociative ? PDO::FETCH_ASSOC : PDO::FETCH_NUM);
			$this->_statement->closeCursor();
			return $result;
		}
		catch(Exception $e)
		{
            error_log('Error in SQL: ' . ($this->_statement instanceof PDOStatement ? $this->_statement->queryString : $this->getText()));
            if (Prado::isDebugMode()) {
                error_log($e->getMessage());
                error_log($e->getTraceAsString());
            }
			throw new TDbException('dbcommand_query_failed',$e->getMessage(),$this->getDebugStatementText());
		}
	}

	/**
	 * Executes the SQL statement and returns the value of the first column in the first row of data.
	 * This is a convenient method of {@link query} when only a single scalar
	 * value is needed (e.g. obtaining the count of the records).
	 * @return mixed the value of the first column in the first row of the query result. False is returned if there is no value.
	 * @throws TDbException execution failed
	 */
	public function queryScalar()
	{
		try
		{
			// Prado::trace('Query Scalar: '.$this->getDebugStatementText(), 'System.Data');
			$this->doExecute();
			$result=$this->_statement->fetchColumn();
			$this->_statement->closeCursor();
			if(is_resource($result) && get_resource_type($result)==='stream')
				return stream_get_contents($result);
			else
				return $result;
		}
		catch(Exception $e)
		{
            error_log('Error in SQL: ' . ($this->_statement instanceof PDOStatement ? $this->_statement->queryString : $this->getText()));
            if (Prado::isDebugMode()) {
                error_log($e->getMessage());
                error_log($e->getTraceAsString());
            }
			throw new TDbException('dbcommand_query_failed',$e->getMessage(),$this->getDebugStatementText());
		}
	}

    /**
     * Executes the SQL statement and returns the first column of the result.
     * This is a convenient method of {@link query} when only the first column of data is needed.
     * Note, the column returned will contain the first element in each row of result.
     * @return array the first column of the query result. Empty array if no result.
     * @throws TDbException execution failed
     * @since 3.1.2
     */
    public function queryColumn()
    {
        $dataReader = $this->query();

        $column = [];
        while (($row = $dataReader->read()) !== false) {
            $column[] = current($row);
        }
        $dataReader->close();
        return $column;
    }
}

