<?php

/**
 * TOracleCommandBuilder class file.
 *
 * @author Marcos Nobre <marconobre[at]gmail[dot]com>
 * @link http://www.pradosoft.com/
 * @copyright Copyright &copy; 2005-2008 PradoSoft
 * @license http://www.pradosoft.com/license/
 * @version $Id: TOracleCommandBuilder.php 2651 2009-05-11 08:48:37Z Christophe.Boulain $
 * @package System.Data.Common
 */

Prado :: using('System.Data.Common.TDbCommandBuilder');

/**
 * TOracleCommandBuilder provides specifics methods to create limit/offset query commands
 * for Oracle database.
 *
 * @author Marcos Nobre <marconobre[at]gmail[dot]com>
 * @version $Id: TOracleCommandBuilder.php 2651 2009-05-11 08:48:37Z Christophe.Boulain $
 * @package System.Data.Common
 * @since 3.1
 */
class TOracleCommandBuilder extends TDbCommandBuilder {

	public $lastUID;
	
	/**
	 * Iterate through all the columns and returns the last insert id of the
	 * first column that has a sequence or serial.
	 * @return mixed last insert id, null if none is found.
	 */
	public function getLastInsertID()
	{
		return $this->lastUID;
	}
	/**
	 * Appends the $where condition to the string "SELECT * FROM tableName WHERE ".
	 * The tableName is obtained from the {@link setTableInfo TableInfo} property.
	 * @param string query condition
	 * @param array condition parameters.
	 * @return TDbCommand query command.
	 */
	public function createFindCommand($where='1=1', $parameters=array(), $ordering=array(), $limit=-1, $offset=-1, $select='*')
	{

		$table = $this->getTableInfo()->getTableFullName();		
		$fields = implode(', ', $this -> getSelectFieldList($select));

		$sql = "SELECT {$fields} FROM {$table}";
		if(!empty($where))
			$sql .= " WHERE {$where}";

		return $this->applyCriterias($sql, $parameters, $ordering, $limit, $offset);
	}

	/**
	 * Creates an insert command for the table described in {@link setTableInfo TableInfo} for the given data.
	 * Each array key in the $data array must correspond to the column name of the table
	 * (if a column allows to be null, it may be omitted) to be inserted with
	 * the corresponding array value.
	 * @param array name-value pairs of new data to be inserted.
	 * @return TDbCommand insert command
	 */
	public function createInsertCommand($data)
	{
		$conn = $this->getDbConnection();
		$tableInfo = $this->getTableInfo();
		$table = $tableInfo->getTableFullName();
//		if (stripos($table,'BE_USERS')!==false) {
//			var_dump($data);
//			die();
//		}
		list($fields, $bindings) = $this->getInsertFieldBindings($data);
		$sql = "INSERT INTO {$table}({$fields}) VALUES ($bindings)";
		
		$prim = $tableInfo->getPrimaryKeys();
		if (count($prim) == 1 && $tableInfo->getColumn($prim[0])->getPHPType() != 'string')
		{
			$this->lastUID = false;
			$primName = '"'.strtoupper(trim($prim[0],'"')).'"';
			$sql .= " RETURNING  {$primName} INTO :lastUID";
			$command = $conn->createCommand($sql);
			$param = [ &$this, 'lastUID' ];
			$command->bindParameter(':lastUID', $this->lastUID,PDO::PARAM_INT,12);
			$tableInfo->getColumn($prim[0])->setSequenceName('lastUID');
		}
		else {
			$command = $conn->createCommand($sql);
		}
		$this->bindColumnValues($command, $data,PDO::PARAM_STR);		
		return $command;
	}

	/**
	 * Create a name-value or position-value if $position=true binding strings.
	 * @param array data for binding.
	 * @param boolean true to bind as position values.
	 * @return string update column names with corresponding binding substrings.
	 */
	protected function getColumnBindings($values, $position=false)
	{		
		$bindings=array();
		foreach(array_keys($values) as $name)
		{
			$column = $this->getTableInfo()->getColumn($name)->getColumnName();
			$bindings[] = $position ? '`'.$column.'` = ?' : '`'.$column.'` = :'.$name;
		}
		return $bindings;
	}

	/**
	 * Bind the name-value pairs of $values where the array keys correspond to column names.
	 * @param TDbCommand database command.
	 * @param array name-value pairs.
	 */
	public function bindColumnValues($command, $values)
	{
		foreach($values as $name=>$value)
		{
			$column = $this->getTableInfo()->getColumn($name);
			if($value === null && $column->getAllowNull())
				$command->bindValue(':'.$name, null, PDO::PARAM_NULL);
			else
			{
				$command->bindValue(':'.$name, $value, $this->getPdoType($value)); //$column->getPHPType());
			}
		}
	}

	/**
	 * @param TDbCommand database command
	 * @param array values for binding.
	 */
	public function bindArrayValues($command, $values)
	{
		if($this->hasIntegerKey($values))
		{
			$values = array_values($values);
			for($i = 0, $max=count($values); $i<$max; $i++)
				$command->bindValue($i+1, $values[$i], $this->getPdoType($values[$i]));
		}
		else
		{
			foreach($values as $name=>$value)
			{
				$prop = $name[0]===':' ? $name : ':'.$name;
				//ChromePhp::log("prop : " . $prop . " value : " . $value);
				$command->bindValue($prop, $value, $this->getPdoType($value));
			}
		}
	}

	/**
	 * Overrides parent implementation. Only column of type text or character (and its variants)
	 * accepts the LIKE criteria.
	 * @param array list of column id for potential search condition.
	 * @param string string of keywords
	 * @return string SQL search condition matching on a set of columns.
	 */
	public function getSearchExpression($fields, $keywords) {
		$columns = array ();
		foreach ($fields as $field) {
			if ($this->isSearchableColumn($this->getTableInfo()->getColumn($field)))
				$columns[] = $field;
		}
		return parent :: getSearchExpression($columns, $keywords);
	}
	/**
	 *
	 * @return boolean true if column can be used for LIKE searching.
	 */
	protected function isSearchableColumn($column) {
		$type = strtolower($column->getDbType());
		return $type === 'character varying' || $type === 'varchar2' || $type === 'character' || $type === 'char' || $type === 'text';
	}

	/**
	 * Overrides parent implementation to use PostgreSQL's ILIKE instead of LIKE (case-sensitive).
	 * @param string column name.
	 * @param array keywords
	 * @return string search condition for all words in one column.
	 */
	/*
	*
	*	how Oracle don't implements ILIKE, this method won't be overrided
	*
	protected function getSearchCondition($column, $words)
	{
		$conditions=array();
		foreach($words as $word)
			$conditions[] = $column.' LIKE '.$this->getDbConnection()->quoteString('%'.$word.'%');
		return '('.implode(' AND ', $conditions).')';
	}
	*/

	/**
	 * Creates an update command for the table described in {@link setTableInfo TableInfo} for the given data.
	 * Each array key in the $data array must correspond to the column name to be updated with the corresponding array value.
	 * @param array name-value pairs of data to be updated.
	 * @param string update condition.
	 * @param array update parameters.
	 * @return TDbCommand update command.
	 */
	public function createUpdateCommand($data, $where, $parameters=array())
	{
		$table = $this->getTableInfo()->getTableFullName();
		if($this->hasIntegerKey($parameters))
			$fields = implode(', ', $this->getColumnBindings($data, true));
		else
			$fields = implode(', ', $this->getColumnBindings($data));

		if (!empty($where))
			$where = ' WHERE '.$where;
		$command = $this->createCommand("UPDATE {$table} SET {$fields}".$where);
		$this->bindArrayValues($command, array_merge($data, $parameters));
		return $command;
	}


	/**
	 *
	 * Different behavior depends on type of passed data
	 * string
	 * 	usage without modification
	 *
	 * null
	 * 	will be expanded to full list of quoted table column names (quoting depends on database)
	 *
	 * array
	 * - Column names will be quoted if used as key or value of array
	 * 	<code>
	 * 	array('col1', 'col2', 'col2')
	 * 	// SELECT `col1`, `col2`, `col3` FROM...
	 * 	</code>
	 *
	 * - Column aliasing
	 * <code>
	 * array('mycol1' => 'col1', 'mycol2' => 'COUNT(*)')
	 * // SELECT `col1` AS mycol1, COUNT(*) AS mycol2 FROM...
	 * </code>
	 *
	 * - NULL and scalar values (strings will be quoted depending on database)
	 * <code>
	 * array('col1' => 'my custom string', 'col2' => 1.0, 'col3' => 'NULL')
	 * // SELECT "my custom string" AS `col1`, 1.0 AS `col2`, NULL AS `col3` FROM...
	 * </code>
	 *
	 * - If the *-wildcard char is used as key or value, add the full list of quoted table column names
	 * <code>
	 * array('col1' => 'NULL', '*')
	 * // SELECT `col1`, `col2`, `col3`, NULL AS `col1` FROM...
	 * </code>
	 * @param mixed $value
	 * @return array of generated fields - use implode(', ', $selectfieldlist) to collapse field list for usage
	 * @since 3.1.7
	 * @todo add support for table aliasing
	 * @todo add support for quoting of column aliasing
	 */
	public function getSelectFieldList($data='*') {
		
		if(is_scalar($data)) {
			$tmp = explode(',', $data);
			$result = array();
			foreach($tmp as $v)
			{
				$result[] = trim($v);
			}
			return $result;
		}

		$bHasWildcard = false;
		$result = array();
		if(is_array($data) || $data instanceof Traversable) {
			$columns = $this->getTableInfo()->getColumns();
			foreach($data as $key=>$value) {
				if($key==='*' || $value==='*') {
					$bHasWildcard = true;
					continue;
				}

				if(strToUpper($key)==='NULL') {
					$result[] = 'NULL';
					continue;
				}

				if(strpos($key, '(')!==false && strpos($key, ')')!==false) {
					$result[] = $key;
					continue;
				}

				if(stripos($key, 'AS')!==false) {
					$result[] = $key;
					continue;
				}

				if(stripos($value, 'AS')!==false) {
					$result[] = $value;
					continue;
				}

				$v = isset($columns[$value]);
				$k = isset($columns[$key]);
				if(is_integer($key) && $v) {
					$key = $value;
					$k = $v;
				}

				if(strToUpper($value)==='NULL') {
					if($k)
						$result[] = 'NULL AS ' . $columns[$key]->getColumnName();
					else
						$result[] = 'NULL' . (is_string($key) ? (' AS ' . (string)$key) : '');
					continue;
				}

				if(strpos($value, '(')!==false && strpos($value, ')')!==false) {
					if($k)
						$result[] = $value . ' AS ' . $columns[$key]->getColumnName();
					else
						$result[] = $value . (is_string($key) ? (' AS ' . (string)$key) : '');
					continue;
				}

				if($v && $key==$value) {
					$result[] = $columns[$value]->getColumnName();
					continue;
				}

				if($k && $value==null) {
					$result[] = $columns[$key]->getColumnName();
					continue;
				}

				if(is_string($key) && $v) {
					$result[] = $columns[$value]->getColumnName() . ' AS ' . $key;
					continue;
				}

				if(is_numeric($value) && $k) {
					$result[] = $value . ' AS ' . $columns[$key]->getColumnName();
					continue;
				}

				if(is_string($value) && $k) {
					$result[] = $this->getDbConnection()->quoteString($value) . ' AS ' . $columns[$key]->getColumnName();
					continue;
				}

				if(!$v && !$k && is_integer($key)) {
					$result[] = is_numeric($value) ? $value : $this->getDbConnection()->quoteString((string)$value);
					continue;
				}

				$result[] = (is_numeric($value) ? $value : $this->getDbConnection()->quoteString((string)$value)) . ' AS ' . $key;
			}
		}

		if($data===null || count($result) == 0 || $bHasWildcard)
			$result = $result = array_merge($this->getTableInfo()->getColumnNames(), $result);
		
		return $result;
	}




	/**
	 * @param mixed PHP value
	 * @return integer PDO parameter types.
	 */
	public static function getPdoType($value)
	{
		switch(gettype($value))
		{
			case 'boolean': return PDO::PARAM_INT;
			case 'integer': return PDO::PARAM_INT;
			case 'string' : return PDO::PARAM_STR;
			case 'NULL'   : return PDO::PARAM_NULL;
			default: PDO::PARAM_STR;
		}
	}

}
?>
