<?php
/**
 * OracleSQLTranslator.php
 *
 * A translator from MySQL dialect into Oracle dialect for Limesurvey
 * (http://www.limesurvey.org/)
 *
 * Copyright (c) 2012, André Rothe <arothe@phosco.info, phosco@gmx.de>
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *
 *   * Redistributions of source code must retain the above copyright notice,
 *     this list of conditions and the following disclaimer.
 *   * Redistributions in binary form must reproduce the above copyright notice,
 *     this list of conditions and the following disclaimer in the documentation
 *     and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT
 * SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 * TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGE.
 */

namespace PHPSQLParser;
use PHPSQLParser;
use PHPSQLParser\utils\ExpressionType;

$_ENV['DEBUG'] = 0;

/**
 * This class enhances the PHPSQLCreator to translate incoming
 * parser output into another SQL dialect (here: Oracle SQL).
 * 
 * @author arothe
 *
 */
class OracleTranslator extends PHPSQLCreator {

    private $con; # this is the database connection from LimeSurvey
    private $preventColumnRefs = array();
    private $allTables = array();
    private $_meta;    
    private $_tmpTables = 0;
    private $_WIDTH = [];
    protected $parser;
    protected $schema;
    const ASTERISK_ALIAS = "[#RePl#]";
    const SCHEMA = "GH43091504DB";

    public function __construct($schema = self::SCHEMA) {
        parent::__construct();
        //$this->_meta = $metadata;
        $this->schema = $schema;
        $this->initGlobalVariables();
    }

    private function initGlobalVariables() {
        $this->preventColumnRefs = [];
        $this->allTables = [];
    }

    public static function dbgprint($txt) {
        //if (isset($_ENV['DEBUG'])) {
            print $txt;
       // }
    }

    public static function preprint($s, $return = false) {
        if (!isset($_ENV['DEBUG'])) return;
        $x = "<pre>";
        $x .= print_r($s, 1);
        $x .= "</pre>";
        if ($return) {
            return $x;
        }
        self::dbgprint($x . "<br/>\n");
    }

    protected function checkColumns(&$select,$group) {
        
        if (!$group) return;
        
//var_dump($select,$group);//die();
        foreach($select as &$v)
        {            
            if ($this->searchGrouppedFunction($v,$group)) continue;
            
            if ($v['expr_type'] === ExpressionType::AGGREGATE_FUNCTION) continue;
            if ($this->findIfChildAggregate($v)) continue;
            
            if ($v['expr_type'] == ExpressionType::SIMPLE_FUNCTION)
            {
                if ($this->searchGrouppedFunction($v,$group)) {
                    //echo "0\n";
                    continue;
                }
                $alias = $v['alias'];
                unset($v['alias']);
                $v = [ 'expr_type' => ExpressionType::AGGREGATE_FUNCTION,
                       'base_expr' => 'FIRST', 'delim' => ',',
                       'alias' => $alias,
                       'sub_tree' => [ $v  ] ];

                continue;                
            }
            
            if ($v['expr_type'] == ExpressionType::COLREF)
            {
                if ($this->searchGrouppedFunction($v,$group) ) {//['no_quotes']['parts']
//echo "1\n";
                    continue;
                }
                $alias = $v['alias'];
                unset($v['alias']);
                $v = [ 'expr_type' => ExpressionType::AGGREGATE_FUNCTION,
                       'base_expr' => 'FIRST', 'delim' => ',',
                       'alias' => $alias,
                       'sub_tree' => [ $v  ] ];

                continue;                
                //print_r($select);
            }
 
            if (is_array($v['sub_tree'])) {
                //echo "deeper ...\n";   
               // $this->checkColumns($v['sub_tree'],$group) ;
            }
                        
        }
                   // echo "0\n";
                    return;
        
    }
    
    protected function findIfChildAggregate($v)
    {
        $v = $v['sub_tree'] ?? null;
        if (!is_array($v)) {
            return false;
        }
        if (array_search(ExpressionType::AGGREGATE_FUNCTION, array_column($v,'expr_type')) !== false) {
            return true;
        }
        foreach(array_column($v,'sub_tree') as $_v) {
            if ($this->findIfChildAggregate($_v)) return true;
        }
        return false;
    }

    protected function searchGrouppedFunction($value,$group)
    {
        unset($value['alias']);
        $s = $this->processFunction($value);
        $s .= $this->processColRef($value);
        $s = strtolower($s);

        foreach($group as $v)        
        {
            if ($s == strtolower(trim($v['base_expr'],'`"'))) return true;
            if ($s == strtolower($this->processFunction($v)) ) return true;
            if ($s == strtolower($this->processColRef($v)) ) return true;
            
        }
        return false;
    }
    protected function searchGrouppedValue($value,$group)
    {
        //$value = str_replace( ['`',' '],'',strtolower($value));
        $naked = (!isset($value[1]));
        echo "check for \n";
        //var_dump($value,$group);
        foreach($group as $v)
        {
            $parts = $v['no_quotes']['parts'];
            var_dump($parts,$value);
            //echo ' '.str_replace( ['`',' '],'',strtolower($v['base_expr'])).';';
            if ($naked)
            {
                if (isset($parts[1]))
                {
                    return $value[0] == $parts[1];
                }
                else
                {
                    return $value[0] == $parts[0];
                }
            }
            else
            {
                if (isset($parts[1]))
                {
                    return ( $value[1] == $parts[1] && $value[0] == $parts[0] );
                }
                else
                {
                    
                    return $value[1] == $parts[0];
                }                
            }
            //if (str_replace( ['`',' '],'',strtolower($v['base_expr'])) == $value) return true;
        }
        return false;        
    }

    protected function processSetExpression($parsed)
    {
        if ($parsed['expr_type'] !== ExpressionType::EXPRESSION) {
            return '';
        }
        $sql = '';
        foreach ($parsed['sub_tree'] as $k => $v) {
            $len = strlen($sql);
            $sql .= $this->processColRef($v);
            $sql .= $this->processConstant($v);
            $sql .= $this->processOperator($v);
            $sql .= $this->processFunction($v);
            $sql .= $this->processSubQuery($v);
            if ($len == strlen($sql)) {
                throw new \Exception();
                throw new PHPSQLParser\exceptions\UnableToCreateSQLException('SET expression subtree', $k, $v, 'expr_type');
            }

            $sql .= ' ';
        }

        $sql = substr($sql, 0, -1);

        return $sql;
    }

    protected function processDeleteStatement($parsed)
    {
        $sql = $this->processDELETE($parsed['DELETE']).' '.$this->processFROM($parsed['FROM']).' ';
            //    .$this->processWHERE($parsed['WHERE']);
        if ($parsed['WHERE']) {
            $sql .= $this->processWHERE($parsed['WHERE']);
        }
        return $sql;
    }

    protected function processUpdateStatement($parsed)
    {
        $sql = $this->processUPDATE($parsed['UPDATE']).' '.$this->processSET($parsed['SET']);
        if (isset($parsed['WHERE'])) {
            $sql .= ' '.$this->processWHERE($parsed['WHERE']);
        }

        return $sql;
    }

    protected function processDELETE($parsed)
    {
        $sql = 'DELETE ';
        if (isset($parsed['TABLES']) && \is_array($parsed['TABLES'])) {
            foreach ($parsed['TABLES'] as $k => $v) {
                $sql .= $v . ',';
            }
        }
        return substr($sql, 0, -1);
    }
    
    protected function processAlias($parsed) {
        if (!$parsed) {
            return "";
        }
        # we don't need an AS between expression and alias
        $sql = " " . '"'.trim($parsed['name'],'`"').'"';
        return $sql;
    }

    protected function XprocessDELETE($parsed) {
        if (count($parsed['TABLES']) > 1) {
            die("cannot translate delete statement into Oracle dialect, multiple tables are not allowed.");
        }
        return "DELETE";
    }

    public static function getColumnNameFor($column) {
        $cols = \FOracleSqlTransform::$transformationArray;
        $column = strtoupper($column);
        if (in_array($column,$cols)) {
            
        }
        $column = ($column == '*') ?  '*' : '"'.trim($column,'`').'"';
        // TODO: add more here, if necessary
        return $column;
    }

    public static function getShortTableNameFor($table) {
        if (strtolower($table) === 'surveys_languagesettings') {
            $table = 'surveys_lngsettings';
        }
        // TODO: add more here, if necessary
        //$table  = ($table) ? '"'.trim($table,'`').'"' : '';
        return strtoupper($table);
    }

    protected function processTable($parsed, $index) {
        if ($parsed['expr_type'] !== 'table') {
            return "";
        }
        //var_dump($parsed);
        $parts = $parsed["no_quotes"]['parts'];
        $alias = strtoupper($this->processAlias($parsed['alias']));
        if (!$parsed["no_quotes"]["delim"]) {
            $tableName = $this->getShortTableNameFor($parsed['table']);

            $sql = '';
            if ($tableName !== 'DUAL') {
                $sql .= '"'.$this->schema.'".';
            }
            $sql .= $tableName;
        }
        else {
            $table = $this->getShortTableNameFor($parts[1]);
            $sql =  '"'.trim($parts[0],'`').'"."'.trim($table,'`').'"';
            $alias = $alias ?: ' "'.trim($parts[1],'`').'"';
        }
        
        
         
        $sql .= $alias;

        if ($index !== 0) {
            $sql = $this->processJoin($parsed) . " " . $sql;
            $sql .= $this->processRefType($parsed['ref_type']);
            $sql .= $this->processRefClause($parsed['ref_clause']);
        }

        # store the table and its alias for later use
        $last = array_pop($this->allTables);
        $last['tables'][] = array('table' => $this->getShortTableNameFor($parsed['table']), 'alias' => trim($alias));
        $this->allTables[] = $last;

        return $sql;
    }

    protected function processFROM($parsed) {
        $this->allTables[] = array('tables' => array(), 'alias' => '');
        return parent::processFROM($parsed);
    }

    protected function processTableExpression($parsed, $index) {
        if ($parsed['expr_type'] !== 'table_expression') {
            return "";
        }
        $sql = substr($this->processFROM($parsed['sub_tree']), 5); // remove FROM keyword
        $sql = "(" . $sql . ")";

        $alias = $this->processAlias($parsed['alias']);
        $sql .= $alias;

        # store the tables-expression-alias for later use
        $last = array_pop($this->allTables);
        $last['alias'] = trim($alias);
        $this->allTables[] = $last;

        if ($index !== 0) {
            $sql = $this->processJoin($parsed) . " " . $sql;
            $sql .= $this->processRefType($parsed['ref_type']);
            $sql .= $this->processRefClause($parsed['ref_clause']);
        }
        return $sql;
    }

    private function getTableNameFromExpression($expr) {
        $pos = strpos($expr, ".");
        if ($pos === false) {
            $pos = -1;
        }
        return trim(substr($expr, 0, $pos + 1), ".");
    }

    private function getColumnNameFromExpression($expr) {
        $pos = strpos($expr, ".");
        if ($pos === false) {
            $pos = -1;
        }
        return substr($expr, $pos + 1);
    }

    private function isCLOBColumnInDB($table, $column) {
        return false;
        $col = $this->_meta->getTableInfo($table)->getColumn($column);
        return (stripos($col->getDbType(),'LOB') === 1 );
    }

    protected function isCLOBColumn($table, $column) {
        $tables = end($this->allTables);

        if ($table === "") {
            foreach ($tables['tables'] as $k => $v) {
                if ($this->isCLOBColumn($v['table'], $column)) {
                    return true;
                }
            }
            return false;
        }

        # check the aliases, $table cannot be empty
        foreach ($tables['tables'] as $k => $v) {
            if ((strtolower($v['alias']) === strtolower($table))
                    || (strtolower($tables['alias']) === strtolower($table))) {
                if ($this->isCLOBColumnInDB($v['table'], $column)) {
                    return true;
                }
            }
        }

        # it must be a valid table name
        return $this->isCLOBColumnInDB($table, $column);
    }

    protected function processOrderByExpression($parsed) {
        if ($parsed['expr_type'] !== 'expression') {
            return "";
        }

        $table = $this->getTableNameFromExpression($parsed['base_expr']);
        $col = $this->getColumnNameFromExpression($parsed['base_expr']);

        //$col = strtoupper($col) == $col ? $col : str_replace('"','');
        $sql = ($table !== "" ? $table . "." : "") . $col;

        # check, if the column is a CLOB
        if ($this->isCLOBColumn($table, $col)) {
            $sql = "cast(substr(" . $sql . ",1,200) as varchar2(200))";
        }

        return str_replace('`','"',$sql . " " . $parsed['direction']);
    }

    protected function processColRef($parsed) {
        if ($parsed['expr_type'] !== 'colref') {
            return "";
        }

        $table = $this->getTableNameFromExpression($parsed['base_expr']);
        $col = $this->getColumnNameFromExpression($parsed['base_expr']);

        # we have to change the column name, if the column is uid
        # we have to change the tablereference, if the tablename is too long
        $col = $this->getColumnNameFor($col);
        $table = $this->getShortTableNameFor($table);

        # if we have * as colref, we cannot use other columns
        # we have to add alias.* if we know all table aliases
        if (($table === "") && ($col === "*")) {
            array_pop($this->preventColumnRefs);
            $this->preventColumnRefs[] = true;
            return self::ASTERISK_ALIAS; # this is the position, we have to replace later
        }

        $alias = "";
        if (isset($parsed['alias'])) {
            $alias = $this->processAlias($parsed['alias']);
        }

        return (($table !== "") ? ($table . "." . $col) : $col) . $alias;
    }

    protected function processFunctionOnSelect($parsed) {
        $old = end($this->preventColumnRefs);
        $sql = $this->processFunction($parsed);

        if ($old !== end($this->preventColumnRefs)) {
            # prevents wrong handling of count(*)
            array_pop($this->preventColumnRefs);
            $this->preventColumnRefs[] = $old;
            $sql = str_replace(self::ASTERISK_ALIAS, "*", $sql);
        }
        return $sql;
    }

    protected function processSELECT($parsed) {
        $this->preventColumnRefs[] = false;

        $sql = "";
        foreach ($parsed as $k => $v) {
            $len = strlen($sql);
            $sql .= $this->processColRef($v);
            $sql .= $this->processSelectExpression($v);
            $sql .= $this->processFunctionOnSelect($v);
            $sql .= $this->processConstant($v);
           // $sql .= $this->processSelectBracketExpression($v);

            if ($len == strlen($sql)) {
                //var_dump($v);
                throw new \Exception();
                $this->stop('SELECT', $k, $v, 'expr_type');
            }

            $sql .= ",";
        }
        $sql = substr($sql, 0, -1);
        return "SELECT " . $sql;
    }

    private function correctColRefStatement($sql) {
        $alias = "";
        $tables = end($this->allTables);

        # should we correct the selection list?
        if (array_pop($this->preventColumnRefs)) {

            # do we have a table-expression alias?
            if ($tables['alias'] !== "") {
                $alias = $tables['alias'] . ".*";
            } else {
                foreach ($tables['tables'] as $k => $v) {
                    $alias .= ($v['alias'] === "" ? $v['table'] : $v['alias']) . ".*,";
                }
                $alias = substr($alias, 0, -1);
            }
            $sql = str_replace(self::ASTERISK_ALIAS, $alias, $sql);
        }
        return $sql;
    }
    
    protected function processGROUP($parsed)
    {
        $sql = '';
        foreach ($parsed as $k => $v) {
            $len = strlen($sql);
            //echo '<pre>';
            //var_dump($this);
            $sql .= $this->processWhereExpression($v);
            $sql .= $this->processFunction($v);
            $sql .= $this->processColRef($v);
            //$sql .= $this->processOrderByAlias($v);

            if ($len == strlen($sql)) {
                var_dump($parsed);
                throw new \Exception();
                throw new PHPSQLParser\exceptions\UnableToCreateSQLException('GROUP', $k, $v, 'expr_type');
            }

            $sql .= ',';
        }
        $sql = substr($sql, 0, -1);

        return 'GROUP BY '.$sql;
    }
    
    
    protected function processLIMIT($parsed)
    {
        $offset = (int) $parsed['offset'];
        $limit  = (int) $parsed['rowcount'];
        if (($limit <= 0) and ($offset <= 0)) return '';
        
        if ($offset == 0) {
            
          return 'FETCH FIRST '.$limit.' ROWS ONLY ';
        }
            
        return 'OFFSET '.$offset.' ROWS FETCH NEXT '.$limit.' ROWS ONLY ';
    }
    
    protected function processWIDTH($parsed,$sql)
    {
          $offset = $parsed['offset'];
          $limit  = $parsed['rowcount'];
		if (($limit < 0) and ($offset < 0)) return '';
          
          $t = ++$this->_tmpTables;
          return "USER_SQL{$t} AS (".$sql."),
                 PAGINATION{$t} AS (SELECT USER_SQL{$t}.*, rownum as rowNumId FROM USER_SQL{$t})";
    }
    
    protected function processLIMITAfter($parsed)
    {
          $offset = $parsed['offset'];
          $limit  = $parsed['rowcount'];
		if (($limit < 0) and ($offset < 0)) return '';

		$filters = array();
		if($offset>0){
			$filters[] = 'rowNumId > '.(int)$offset;
		}

		if($limit>=0){
			$filters[]= 'rownum <= '.(int)$limit;
		}

		if (count($filters) > 0){
			$filter = implode(' and ', $filters);
			$filter= " WHERE ".$filter;
		}else{
			$filter = '';
		}

          $t = $this->_tmpTables;
          return "SELECT * FROM PAGINATION{$t} {$filter}";
    }

    protected function processSelectStatement($parsed,$inUnion=false) {
                
        $this->checkColumns($parsed['SELECT'],$parsed['GROUP'] ?? null);
        
        
        //echo "aaa";die();
        
        $sql = $this->processSELECT($parsed['SELECT']);
        $from = $this->processFROM($parsed['FROM']);

        # correct * references with tablealias.*
        # this must be called after processFROM(), because we need the table information
        $sql = $this->correctColRefStatement($sql) . " " . $from;

        if (isset($parsed['WHERE'])) {
            $sql .= " " . $this->processWHERE($parsed['WHERE']);
        }
        if (isset($parsed['GROUP'])) {
            $sql .= " " . $this->processGROUP($parsed['GROUP']);
        }
        
        
        if (isset($parsed['ORDER']) && !$inUnion) {
            $sql .= " " . $this->processORDER($parsed['ORDER']);
        }

        if (isset($parsed['LIMIT'])) {
            //$this->_WIDTH[] = $this->processWIDTH($parsed['LIMIT'],$sql);
            //$sql = $this->processLIMITAfter($parsed['LIMIT']);
            $sql .= ' '.$this->processLIMIT($parsed['LIMIT']);
        }

        # select finished, we remove its tables
        #  FIXME: we should add it to the previous tablelist with the
        #  global alias, if such one exists
        array_pop($this->allTables);

        return $sql;
    }
    
    protected function getDateFormat($i)
    {
        $formats = \FOracleSqlTransform::$dateFormats;
        return $formats[$i] ?: 'YYYY-MM-DD HH24:MI:SS';
    }
    
    protected function wrapDate($sql)
    {
            $d = trim($sql,"'");
            if (preg_match('/_asdate([0-9])$/',$d,$m)) {
                return "TO_DATE(".$sql.",'".$this->getDateFormat($m[1])."')";
            }
            elseif (!is_numeric($d))
            {
                if ( \DateTime::createFromFormat('Y-m-d H:i:s',$d) )
                {
                    return "TO_DATE(".$sql.",'YYYY-MM-DD HH24:MI:SS')";
                }
                elseif ( \DateTime::createFromFormat('Y-m-d H:i',$d) )
                {
                    return "TO_DATE(".$sql.",'YYYY-MM-DD HH24:MI')";
                }
                elseif ( \DateTime::createFromFormat('Y-m-d',$d))
                {
                    return "TO_DATE(".$sql.",'YYYY-MM-DD')";
                }
            }
            return $sql;
        
    }
    
    protected function convertDatePattern($pat) {
        if (preg_match('/(%Y-?%m-?%d)|(%H:%i:?%s?)|(%x-%v)/',$pat))
        {
            $pat = str_replace(['%Y','%d','%m','%H','%i','%s','%x','%v'],['YYYY','DD','MM','HH24','MI','SS','YYYY','IW'],$pat);
        }
        return $pat;        
    }

    
    protected function processConstant($parsed)
    {
        $sql = parent::processConstant($parsed);
        if ($sql && $sql[0] == '"')
        {
            $sql = "'".trim($sql,'"\'')."'";
        }
        if ($parsed['expr_type'] === ExpressionType::CONSTANT) {
            
            $sql = $this->convertDatePattern($sql);
            return $this->wrapDate($sql);
            
        }
        
        if ($parsed['expr_type'] !== ExpressionType::PARAM) {
            return $sql;
        }
        
        $cols = \FOracleSqlTransform::$transformationArray;
        $param = ltrim($sql,':');
        if (in_array(strtoupper($param),$cols))
        {
            $sql = ':Or_'.$param;
        }
        
        return $this->wrapDate($sql);
    }
    
    
    protected function processJoin($parsed)
    {
        $refType = $parsed['ref_type'];
        $parsed = $parsed['join_type'];
        
        if ($parsed === 'CROSS') {
            return 'CROSS JOIN';
        }
        if ($parsed === 'JOIN') {
            return ($refType == 'ON') ? 'INNER JOIN' : 'CROSS JOIN';
        }
        if ($parsed === 'LEFT') {
            return 'LEFT JOIN';
        }
        if ($parsed === 'RIGHT') {
            return 'RIGHT JOIN';
        }
        // TODO: add more
        throw new PHPSQLParser\exceptions\UnsupportedFeatureException($parsed);
    }
    
    
    public function customProcessLISTAGG($parsed)
    {
        $orderField = null;
        $parsedNew = $parsed; $i = 0;
        $root = $parsed['sub_tree'][0];
        if ($root['expr_type'] === ExpressionType::EXPRESSION )
        {
            if ($root['sub_tree'][0]['base_expr'] === 'DISTINCT') {
                $parsedNew['sub_tree'][0] = $root['sub_tree'][1];
                ++$i;
            }            
        }
        //var_dump($parsedNew['sub_tree']);
        $root = $parsed['sub_tree'][$i];
        if (true || $root['expr_type'] === ExpressionType::EXPRESSION  )
        {         
            $order = strripos($parsedNew['sub_tree'][0]['base_expr'],'ORDER');
            while ($order && count($parsedNew['sub_tree'][0]['sub_tree'])>1) {
                $item = array_pop($parsedNew['sub_tree'][0]['sub_tree']);
                if ($item['expr_type'] === ExpressionType::COLREF ) {
                    $orderField = $item;
                }
            }
            
        }
        array_unshift($parsedNew['sub_tree'], $parsedNew['sub_tree'][$i]);
        array_pop($parsedNew['sub_tree']);
        
        if (count($parsedNew['sub_tree'])==1) {
            array_push($parsedNew['sub_tree'], [ 'base_expr' => "','",  'expr_type' => ExpressionType::CONSTANT ]);
        }
        $sql = parent::processFunction($parsedNew).' WITHIN GROUP ';

        //var_dump($sql,$parsedNew['sub_tree'][0]);die();
        
        if ($orderField)
        {
            $p = $this->processColRef($orderField);
            $sql .= "( ORDER BY $p )";
        }
        else
        {
            $sql .= '( ORDER BY ';
        //    $sql .= $this->processExpression($parsedNew['sub_tree'][0]);
            $sortColumn = $this->processColRef($parsedNew['sub_tree'][0]);
            if (empty($sortColumn)) {
                $sortColumn = 1;
            }
            $sql .= $sortColumn;
            //$sql .= $this->processFunction($parsedNew['sub_tree'][0]);
            $sql .= ") ";
            //$sql .= $this->processAlias($parsed['alias']);
          //  $sql .= $this->processFunction($parsedNew['sub_tree'][0]);
        }
        return $sql;
        
    }
    
    public function processDateFormat($format) {
        $fm = \FOracleSqlTransform::$dFormats;

        return str_replace(array_keys($fm),array_values($fm),$format);
    }
    public function customProcessTO_CHARDATE($parsed)
    {
        $p = [];
        foreach($parsed['sub_tree'] as $k=>$v) {
            $a = $this->processColRef($v);
            $a .= $this->processOperator($v);
            $a .= $this->processFunction($v);
            $a .= $this->processConstant($v);
            $p[] = $this->processDateFormat($a);                        
        }
        return "TO_CHAR({$p[0]},{$p[1]})";
    }

    public function customProcessFIRST($parsed)
    {
        $substring = str_ireplace('FIRST(','LISTAGG(',$this->customProcessLISTAGG($parsed));
        return 'NVL(SUBSTR(' . $substring . ", 1, INSTR(" . $substring . ", ',', 1, 1) - 1), " . $substring . ")";
    }

    public function customProcessDECODE($parsed)
    {
        $p = [];
        foreach($parsed['sub_tree'] as $k=>$v) {
            $a = $this->processColRef($v);
            $a .= $this->processOperator($v);
            $a .= $this->processFunction($v);
            $a .= $this->processConstant($v);
            $p[] = $a;
        }
        return "DECODE( {$p[0]}, '', {$p[2]}, {$p[1]} ) ";  

    }

    public function customProcessNVL($parsed)
    {
        $p = [];
        foreach($parsed['sub_tree'] as $k=>$v) {
            $a = $this->processColRef($v);
            $a .= $this->processOperator($v);
            $a .= $this->processFunction($v);
            $a .= $this->processConstant($v);
            $p[] = $a;
        }
        //$param = ($p[1] == '') ? ' ' : $p[1];
        return "NVL( {$p[0]} , {$p[1]}) ";  

    }

    public function customProcessTRIM_E($parsed)
    {
        $p = [];
        foreach($parsed['sub_tree'] as $k=>$v) {
            $a = $this->processColRef($v);
            $a .= $this->processOperator($v);
            $a .= $this->processFunction($v);
            $a .= $this->processConstant($v);
            $p[] = $a;
        }
        return "TRIM( BOTH ' ' FROM {$p[0]} ) ";  

    }

    public function customProcessEXTRACT_M($parsed)
    {
        $p = [];
        foreach($parsed['sub_tree'] as $k=>$v) {
            $a = $this->processColRef($v);
            $a .= $this->processOperator($v);
            $a .= $this->processFunction($v);
            $a .= $this->processConstant($v);
            $p[] = $a;
        }
        return "EXTRACT( MONTH FROM {$p[0]} ) ";  

    }
    
    public function customProcessEXTRACT_Y($parsed)
    {
        $p = [];
        foreach($parsed['sub_tree'] as $k=>$v) {
            $a = $this->processColRef($v);
            $a .= $this->processOperator($v);
            $a .= $this->processFunction($v);
            $a .= $this->processConstant($v);
            $p[] = $a;
        }
        return "EXTRACT( YEAR FROM {$p[0]} ) ";  

    }

    public function customProcessCASE($parsed)
    {
        $p = [];
        foreach($parsed['sub_tree'] as $k=>$v) {
            if ($k==0)
            {
                if ($v['expr_type'] === ExpressionType::EXPRESSION ||
                    $v['expr_type'] === ExpressionType::BRACKET_EXPRESSION )
                    $op = '';
                else
                    $op = "IS NOT NULL";
            }
            $a = $this->processColRef($v);
            //$a .= $this->processOperator($v);
            $a .= $this->processFunction($v);
            $a .= $this->processConstant($v);
            $a .= $this->processSelectExpression($v);
            $a .= $this->processWhereBracketExpression($v);
            $a .= $this->processSubquery($v);
            $p[] = $a;
        }
        return "CASE WHEN {$p[0]} $op THEN {$p[1]} ELSE {$p[2]} END ";  

    }
    
    public function customProcessCONCAT($parsed,$sep='')
    {
        $p = [];
        foreach($parsed['sub_tree'] as $k=>$v) {
            $a = $this->processColRef($v);
            $a .= $this->processOperator($v);
            $a .= $this->processFunction($v);
            $a .= $this->processConstant($v);
            $p[] = $a;
            if ($sep) {
                $p[] = $sep;    
            }
        }
        if ($sep) {
            array_pop($p);
        }
        return '('.implode(" || ",$p).')';  
    }

    public function customProcessCONCATWS($parsed)
    {
        $first = array_shift($parsed['sub_tree']);
        $sep = $this->processConstant($first);//['base_expr'];
        return $this->customProcessCONCAT($parsed,$sep);
    }
    
    public function customProcessDATEADD($parsed,$op='+')
    {
        $p = [];
        foreach($parsed['sub_tree'] as $k=>$v) {
            $a = $this->processFunction($v);
            $a .= $this->processConstant($v);
            $a .= $this->processColRef($v);
            if ($v['expr_type'] === ExpressionType::EXPRESSION) {
                foreach($v['sub_tree'] as $k1 => $v1)
                {
                    $a .= $this->processReserved($v1);
                    $a1 = $this->processConstant($v1);
                    $a .= ( $a1 ? " '".$a1."' " : "" );
                }
            }
            $p[] = $a;
        }
        return '('.implode(" $op ",$p).')';  
    }
    
    public function customProcessDATESUB($parsed)
    {
        return $this->customProcessDATEADD($parsed,'-');  
    }
    
    public function processFunction($parsed) {
        
        $fn = \FOracleSqlTransform::$functions;
        $name = $parsed['base_expr'];
        $sql = '';
        if ($parsed['expr_type'] === ExpressionType::SIMPLE_FUNCTION || $parsed['expr_type'] === ExpressionType::AGGREGATE_FUNCTION)
        {
            if (isset($fn[strtolower($name)]))
            {
                if (!$parsed['sub_tree']) {                    
                    return $fn[strtolower($name)];
                }
                $parsed['base_expr'] = $fn[strtolower($name)];
            }
        
            $customFn = 'customProcess'.$parsed['base_expr'];
            if (in_array($customFn,get_class_methods($this))) {
                $sql = $this->$customFn($parsed);
            }
            else {
                $sql = parent::processFunction($parsed);            
            }
            
            $sql .= $this->processAlias($parsed['alias'] ?? null);
        }
        return $sql;
    }
    
    
    protected function processWhereBracketExpression($parsed)
    {        
        if ($parsed['expr_type'] !== ExpressionType::BRACKET_EXPRESSION) {
            return '';
        }

        if (stripos($parsed['base_expr'],'regexp') === false) {
            return parent::processWhereBracketExpression($parsed);
        }
        
        //look for regexp or rlike operator
        $nv = []; $args = [];
        foreach ($parsed['sub_tree'] as $k => $v)
        {
            if ($v['expr_type'] === ExpressionType::BRACKET_EXPRESSION && stripos($v['base_expr'],'regexp') !== false) {
                return parent::processWhereBracketExpression($parsed);
            }
            if ($v['expr_type'] === ExpressionType::OPERATOR)
            {   if (strtolower($v['base_expr']) == 'not')
                {
                    $nv[] = $v;
                }
            }
            else
            {
                $args[] = $v;
            }            
        }

        //$args[] = 'i';
        //$args[0] = 'CONVERT('.$args[0].'';
        
        //$args[0] = iconv('utf8','ascii',$args[1] );
        $nv[] = [ 'expr_type' => ExpressionType::SIMPLE_FUNCTION, 
                  'base_expr'=> 'REGEXP_LIKE',
                  'sub_tree' => $args ];

        $parsedNew = $parsed;
        $parsedNew['sub_tree'] = $nv;
        $sql = parent::processWhereBracketExpression($parsedNew);
        //echo $sql."\n";
        return $sql;
    }
  protected function processINSERT($parsed)
    {
        $i = 0;
        if ($parsed[0]['expr_type'] === ExpressionType::TABLE)
        {
            $table = $parsed[$i];
        }
        else {
            ++$i;
            $table = $parsed[$i];
        }
        $sql = 'INSERT INTO '.$this->processTable($table,0);
        ++$i;

        if ($parsed[$i]['expr_type'] !== ExpressionType::COLUMN_LIST) {
            return $sql;
        }

        $columns = '';
        foreach ($parsed[$i]['sub_tree'] as $k => $v) {
            $len = strlen($columns);
            $columns .= $this->processColRef($v);

            if ($len == strlen($columns)) {
                throw new UnableToCreateSQLException('INSERT[columns]', $k, $v, 'expr_type');
            }

            $columns .= ',';
        }

        if ($columns !== '') {
            $columns = ' ('.substr($columns, 0, -1).')';
        }

        $sql .= $columns;

        return $sql;
    }
    
    protected function processORDER($parsed)
    {
        $sql = '';
        foreach ($parsed as $k => $v) {
            $len = strlen($sql);
            $sql .= $this->processOrderByAlias($v) ?: $this->processColRef($v);
            $sql .= $this->processDirection($v['direction']);

            if ($len == strlen($sql)) {
                var_dump($parsed);
                throw new \Exception();
                throw new PHPSQLParser\exceptions\UnableToCreateSQLException('ORDER', $k, $v, 'expr_type');
            }

            $sql .= ',';
        }
        $sql = substr($sql, 0, -1);

        return 'ORDER BY '.$sql;
    }
    
    protected function processOrderByAlias($parsed)
    {
        if ($parsed['expr_type'] === ExpressionType::BRACKET_EXPRESSION) {

            return $this->processSelectBracketExpression($parsed);
        }
        if ($parsed['expr_type'] === ExpressionType::SIMPLE_FUNCTION || $parsed['expr_type'] === ExpressionType::AGGREGATE_FUNCTION) {

            return $this->processFunction($parsed);
        }
        if ($parsed['expr_type'] !== ExpressionType::ALIAS && strpos($parsed['base_expr'],'`') === FALSE) {
                return '';
        }
        else {
            $sql = '"'.trim($parsed['base_expr'],'`"').'"';
        }


        return $sql;
    }

    protected function processUPDATE($parsed)
    {
        return 'UPDATE '.$this->processTable($parsed[0],0);
    }
    
    
    protected function processUnionStatement($parsed) {
    
        $unionType = isset($parsed['UNION ALL']) ? 'UNION ALL' : 'UNION';

        $sql = '';
        if (isset($parsed['LIMIT'])) {
            $sql .= $this->processLIMITBefore($parsed['LIMIT']);
        }
        
        foreach($parsed[$unionType] as $v)
        {
            $sql .= ' ( '.$this->processSelectStatement($v,true).' ) ';
            $sql .= ' '.$unionType;            
        }
        
        $sql = substr($sql,0,-strlen($unionType));
        
        if (isset($parsed['ORDER'])) {
            $sql .= $this->processORDER($parsed['ORDER']);
        }
        
        if (isset($parsed['LIMIT'])) {
            $sql .= $this->processLIMITAfter($parsed['LIMIT']);
        }
        
        return $sql;
        
    }
    
    
    public function create($parsed) {
        $k = key($parsed);
        switch ($k) {
        case "USE":
        # this statement is not an Oracle statement
            $this->created = "";
            break;

        default:
            $this->created = $this->oldCreate($parsed);
            break;
        }
        return $this->created;
    }
    
    public function oldCreate($parsed)
    {
      //  var_dump($parsed);die();
        $k = key($parsed);
        //print_r($parsed);die();
        switch ($k) {

        case 'UNION':
        case 'UNION ALL':
            $this->created = $this->processUnionStatement($parsed);
            break;
        case 'SELECT':
            $sql = $this->processSelectStatement($parsed);
            $width = (!empty($this->_WIDTH)) ? 'WITH '.implode(",\n",$this->_WIDTH)." \n" : '';
            $this->created = $width.$sql;
            break;
        case 'INSERT':
            $this->created = $this->processInsertStatement($parsed);
            break;
        case 'DELETE':
            $this->created = $this->processDeleteStatement($parsed);
            break;
        case 'UPDATE':
            $this->created = $this->processUpdateStatement($parsed);
            break;
        default:
            throw new PHPSQLParser\exceptions\UnsupportedFeatureException($k);
            break;
        }

        return $this->created;
    }
    
    public function process($sql) {
        //self::dbgprint($sql );
        $sqls = explode('/***/',$sql);
        if (count($sqls)>1) {
            $sep = 1;
        }
        else {
            $sep = 2;
            $sqls = explode(';',$sql);
        }
        
        foreach($sqls as &$sql) {
            if (!$sql) continue;
            $this->initGlobalVariables();
            $isInsertIgnore = (strpos($sql, 'INSERT IGNORE') !== false);
            $sql = str_replace(['SQL_CALC_FOUND_ROWS', 'INSERT IGNORE'], ['/* SQL_CALC_FOUND_ROWS */', 'INSERT'],$sql);
            $this->parser = new \PHPSQLParser\PHPSQLParser();
            //$this->parser->addCustomFunction('ON DUPLICATE KEY UPDATE');
            $this->parser->parse($sql);
           // self::preprint($parser->parsed);
            $sql = $this->oldCreate($this->parser->parsed);
            $sql = str_replace(['`',
                                '"/*',
                                '*/"',
                                '/* SQL_CALC_FOUND_ROWS */',
                                "!= ''",
                                "= ''"],
                               ['"',
                                '/*',
                                '*/',
                                'COUNT(*) OVER() AS FOUNDROWS,',
                                'IS NOT NULL',
                                'IS NULL' ],$sql);

            if ($isInsertIgnore) {

                $tableName = strtoupper($this->parser->parsed['INSERT'][1]['table']);
                $tableName = trim($tableName, '`');

                $sql = str_replace(
                    'INSERT',
                    'INSERT /*+ ignore_row_on_dupkey_index(' . $tableName . ', ' . $tableName . '_PK) */',
                    $sql
                );
            }
        }
        if ($sep == 1) {
            return  preg_replace('/VALUES *SELECT/',' SELECT',implode(' ',$sqls));
        }
        else {
            return  implode('; ',$sqls);    
        }
        
    }
    
    public static function ex() {
        $start = microtime(true);
        echo $start."\n";
        //\Prado::switchUser('super');
        //$meta = \TDbMetaData::getInstance(\AddressRecord::getActiveDbConnection());
        $o = new self();
//        echo get_class($o)."\n";
        //$sql = \EventStatsRecord::SQL_DETAIL_CID;
        $sql = "SELECT GROUP_CONCAT(DISTINCT ',',uid) as gc,IF((Name regexp 'abv'),1,0) gc, 
                       CONCAT_WS(\"+\",Name,uid,created) as hh
                       FROM be_users WHERE created > DATE_ADD(NOW(), INTERVAL 2  MONTH) AND CConfirmed > 0 AND start > CURDATE() AND IFNULL(Name,123) AND
                ((Name not regexp _utf8 'wer') AND DATE_FORMAT(s,'%Y-%m-%d %H:%i:%s')  = '2016-06-01 10:00:00') AND uid = :uid  ORDER BY position_id DESC LIMIT 1 /* ORACLE */";
//        $o->process(str_ireplace(['SQL_CALC_FOUND_ROWS','USE INDEX (client_id)','USE INDEX (client)','%where%'],['','','','(1=1)'],\AddressViewRecord::getFullSQL()));
        $sql = "INSERT INTO BE_USERS (uid,Name,created) values (?,2,2)";// RETURNING uid INTO :lastID";
        $sql = "SELECT d.*
							    FROM  f_legal_docs d
									LEFT JOIN
									f_legal_doc_users du ON d.uid = du.f_legal_docs_id AND du.be_users_id = ?
							    WHERE du.ts IS NULL AND d.end IS NULL OR d.end > NOW() GROUP BY d.name ORDER BY `version` DESC LIMIT 1";
       $sql2 = "SELECT COUNT(*) FROM `user_addresses` WHERE user_id  IN (SELECT t.uid as r FROM f_users_tree t,f_users_tree tu WHERE tu.uid IN (:EXTRACTED0)
       AND t.lft >= tu.lft AND t.rgt <= tu.rgt) AND `type` IS NULL AND create_date >= DATESUB(CURDATE(), INTERVAL 1 DAY)";

       $sql1 = "SELECT t.*,
            IF(t.owner_id != :uid, NULL, (SELECT COUNT(*) FROM cal_resources_has_users WHERE cal_resources_id = t.uid AND is_request > 0)) as requests_count,
            IF(t.owner_id != :uid, NULL, (SELECT MAX(`created`) FROM cal_resources_has_users WHERE cal_resources_id = t.uid  AND is_request > 0)) as last_request_date,
            IFNULL(f.resource_id, 0) as my_favorite
            FROM cal_resources t
            LEFT JOIN cal_resources_favorites f
            ON t.uid = f.resource_id AND f.be_user_id = :uid
            WHERE owner_id = :uid 
            OR uid IN (SELECT cal_resources_id FROM cal_resources_has_users WHERE be_users_id = :uid AND is_request = 0) 
            OR uid IN (SELECT cal_resources_id FROM cal_resources_has_groups WHERE groups_id IN (SELECT groups_id FROM groups_has_users WHERE be_users_id = :uid))
            
";
$sql3 = "select ua.uid AS uid, CONCAT(ua.firstname, ' ', ua.surname) AS name, c.value as phone
                                            FROM user_addresses ua
                                            LEFT JOIN contacts c
                                            ON ua.uid = c.user_address_id AND c.`type` = 'Phone'
                                            WHERE ua.type IS NULL AND ( ua.user_id = IF('%filter%','%filter%',':USER_UID')
                                            AND ua.user_id  IN (SELECT t.uid as r FROM f_users_tree t,f_users_tree tu WHERE tu.uid IN (:EXTRACTED0) AND t.lft >= tu.lft AND t.rgt <= tu.rgt)
                                             AND  (1=1) )
                                            GROUP BY `ua`.`uid`, ua.firstname, ua.surname
                                            ORDER BY `name`
";

$sql4 = "SELECT t.*,
            IF(t.owner_id != :uid, NULL, (SELECT COUNT(*) FROM cal_resources_has_users WHERE cal_resources_id = t.uid AND is_request > 0)) as requests_count,
            IF(t.owner_id != :uid, NULL, (SELECT MAX(`created`) FROM cal_resources_has_users WHERE cal_resources_id = t.uid  AND is_request > 0)) as last_request_date,
            IFNULL(f.resource_id, 0) as my_favorite
            FROM cal_resources t
            LEFT JOIN cal_resources_favorites f
            ON t.uid = f.resource_id AND f.be_user_id = :uid
            WHERE ((owner_id = :uid) 
            OR uid IN (SELECT cal_resources_id FROM cal_resources_has_users WHERE be_users_id = :uid AND is_request = 0) 
            OR uid IN (SELECT cal_resources_id FROM cal_resources_has_groups WHERE groups_id IN (SELECT groups_id FROM groups_has_users WHERE be_users_id = :uid)))
";
/*
$sql6 = str_replace('$',':',"SELECT  ee.* 
                                    FROM f_events ee 
                                    WHERE ee.user_id IN ( $userIN ) 
                                    AND  (ee.start >= :start AND ee.start <= :end ) 
            
                            UNION
              
                            SELECT  ee.*
                                    FROM f_events ee 
                                    WHERE ee.user_id IN ( $userIN ) 
                                    AND  (ee.end >= :start AND ee.end <= :end AND ee.start < :start )
            
                            UNION
                            SELECT  ee1.* 
                            FROM    f_events ee1, events_has_users eu 
                                    WHERE ee1.uid = eu.f_events_id 
                                    AND     eu.be_users_id IN ( $userIN )
                                    AND   ( ee1.start >= :start AND ee1.start <= :end )
                    
                            UNION
            
                            SELECT  ee1.*
                                    FROM    f_events ee1, events_has_users eu 
                                    WHERE ee1.uid = eu.f_events_id 
                                    
                                    AND eu.be_users_id IN ( $userIN )
                                    AND  (ee1.end >= :start AND ee1.end <= :end AND ee1.start < :start)
                    
                            UNION
            
                            SELECT  ee2.*   
                                    FROM    f_events ee2, events_has_groups eg,groups_has_users gu 
                                            WHERE  ee2.uid = eg.f_events_id 
                                            AND gu.groups_id = eg.groups_id 
                                            AND gu.be_users_id IN ( $userIN )




                                            AND (ee2.start >= :start AND ee2.start <= :end )
            
                            UNION
            
                            SELECT  ee2.*   
                                    FROM    f_events ee2, events_has_groups eg, groups_has_users gu 
                                    WHERE  ee2.uid = eg.f_events_id
                                    AND gu.groups_id=eg.groups_id 
                                    AND gu.be_users_id IN ( $userIN )
                                    AND  (ee2.end >= :start AND ee2.end <= :end AND ee2.start < :start");

*/
$sql5 = "SELECT
                    
    
                    rr.*,

                    (   SELECT  ehus.status
                                FROM events_has_users_status ehus 
                                WHERE ehus.be_users_id = :userIN
                                AND ehus.f_events_id = rr.uid
                    )   as  Status,

                    (   SELECT  IF ( :uid = rr.user_id, IFNULL(rr.rem,-1), IFNULL(ehus.rem,-1))
                                FROM events_has_users_status ehus 
                                WHERE ehus.be_users_id = :uid
                                AND ehus.f_events_id = rr.uid
                    )   as  Rem ,
    
                    CONCAT_WS ( ',',  (SELECT  group_concat(ehs.be_users_id )
                                FROM events_has_users ehs 
                                WHERE rr.uid IN (ehs.f_events_id)
                                GROUP BY ehs.f_events_id        ) ,
                        (SELECT  group_concat(ghu.be_users_id )
                                FROM events_has_groups ehg , groups_has_users ghu 
                                WHERE ehg.groups_id = ghu.groups_id  
                                AND ehg.f_events_id IN (rr.uid)
                                GROUP BY rr.uid)
                    )   as  attendees,
    
                    ( SELECT max(chu.access_level) FROM     f_calendars_has_users chu 
                                                                                                    WHERE chu.user_id  =  :uid
                                                                                                    AND chu.calendar_id IN (rr.user_id)
                                                                                                    AND chu.is_request = 0
                      )    as  Visibility,
       
                    IFNULL((SELECT 3 FROM   f_users_tree t,f_users_tree tu 
                                            WHERE t.uid IN (:inss) 
                                            AND tu.uid IN (:uid) 
                                            AND t.lft >= tu.lft 
                                            AND t.rgt <= tu.rgt    
                            ),
                           0) as ForceSharing,

       
                    CONCAT_WS( ','
                                , (SELECT   GROUP_CONCAT( concat('U', LPAD(ehu.be_users_id, 6, 0)))
                                            FROM events_has_users ehu
                                            WHERE ehu.f_events_id=rr.uid 
                                            GROUP BY ehu.f_events_id)
                                , (SELECT   GROUP_CONCAT( concat('G', LPAD(ehg.groups_id, 6, 0)))
                                            FROM events_has_groups ehg 
                                            WHERE ehg.f_events_id=rr.uid 
                                            GROUP BY ehg.f_events_id) 
                             ) as EntitiesList,
    
                    (   SELECT  concat(r.rule, '|' , r.rdate)
                                FROM f_rrules r
                                WHERE rr.rrule_id = r.uid
                    ) as rule,

                    (   SELECT  CONCAT(TRIM(firstname) , ' ' , TRIM(surname))
                        FROM user_addresses ua
                        WHERE ua.uid = rr.address_id
                    ) as ClientName,

                    (   SELECT TRIM(c.value) as ClientPhone FROM contacts c, user_addresses_visibility uav 
                                                            WHERE c.user_address_id = uav.user_addresses_id
                                                            AND c.type like 'Phone'
                                                            AND c.user_address_id = rr.address_id
                                                            AND (uav.valid_to IS NULL or uav.valid_to > now())
                                                            LIMIT 1
                    ) as ClientPhone
    
                    FROM (SELECT 1) as `rr` order by `rr`.`user_id` , `rr`.`uid`, `rr`.`start`, `rr`.`end`, `rr`.`cid`";
         //$o->process(str_ireplace(['/* SQL_CALC_FOUND_ROWS */','USE INDEX (client_id)','USE INDEX (client)','%where%'],['','','','(1=1)'],$sql));
         //echo $o->process(str_replace('`','',$sql));
         
$sql7 = "select SQL_CALC_FOUND_ROWS  `u`.`uid` AS `uid`,     IF(`fus`.`brokerage`,          `fus`.`brokerage`     , `u`.`provize`) AS `provize`,     IF(`fus`.`bj`,         `fus`.`bj`,     `u`.`bj`) AS `bj`,     (SELECT COUNT(*) FROM future_uk_sestavy WHERE uk_sestavy_id = u.uid AND effective_date <= IF('2016-07', TO_DATE('2016-07','YYYY-MM'), NOW()) GROUP BY uk_sestavy_id) as year,     IFNULL(`b`.`bj_diff`,0) AS `bj_diff`,     `u`.`parameterA` AS `parameterA`,     `u`.`parameterB` AS `parameterB`,     `u`.`parameter1` AS `parameter1`,     `u`.`parameter2` AS `parameter2`,     `u`.`parameter3` AS `parameter3`,     `u`.`parameter4` AS `parameter4`,     `u`.`note` AS `note`,     `u`.`date` AS `date`,     `u`.`approval_date` AS `approval_date`,     `u`.`send_date` AS `send_date`,     `u`.`esl_date` AS `esl_date`,     `u`.`paid_date` AS `paid_date`,             `u`.`FK_products_name` AS `FK_products_name`,     `u`.`FK_products_type` AS `FK_products_type`,     `u`.`FK_pozice` AS `FK_pozice`,     `pn`.`company_name` AS `company_name`,     `pt`.`name` AS `product_name` ,         `pt`.`uid` AS `ProductTypeId`,     `u`.`FK_be_users` AS `FK_be_users`,         CONCAT_WS(' ',a.firstname,a.surname) AS ClientName     FROM  ((`uk_sestavy` `u`,`products_names` `pn`,`products_types` `pt` )         LEFT JOIN bcalc_user_diff_bj b ON `b`.user_id = `u`.FK_be_users AND b.manager_id = :EXTRACTED0 AND b.for_month = '2016-07')         LEFT JOIN user_addresses a ON a.uid = u.parameter3
LEFT JOIN f_view_fus_past AS fus
ON `fus`.`uk_sestavy_id` = `u`.`uid`     WHERE `pn`.`uid`=`u`.`FK_products_name` AND `pn`.`FK_products_type`=`pt`.`uid` AND  ( ((u.parameter3 = ':%filter%')) )   AND (u.FK_be_users  IN (SELECT t.uid as r FROM f_users_tree t,f_users_tree tu WHERE tu.uid IN (:EXTRACTED0) AND t.lft >= tu.lft AND t.rgt <= tu.rgt))";
$sql8 = "SELECT DATE_FORMAT(future_uk_sestavy.effective_date,'%m-%M') as month, YEAR(future_uk_sestavy.effective_date) as year, SUM(future_uk_sestavy.brokerage) as brokerage, SUM(future_uk_sestavy.bj) as bj, uk_sestavy.FK_be_users as user_id FROM future_uk_sestavy LEFT JOIN uk_sestavy ON uk_sestavy.uid = future_uk_sestavy.uk_sestavy_id WHERE uk_sestavy.FK_be_users = :userId AND paid = 0
         GROUP BY DATE_FORMAT(future_uk_sestavy.effective_date,'%m-%M'), YEAR(future_uk_sestavy.effective_date), uk_sestavy.FK_be_users";
$sql8 = "SELECT SQL_CALC_FOUND_ROWS 
    (SELECT e.uid FROM f_events e Where e.address_id = ee.address_id AND e.start > NOW() ORDER BY e.start LIMIT 1)
FROM
    f_events ee
WHERE
    ee.start > NOW() AND ee.address_id IS NOT NULL
GROUP BY ee.address_id";
$sql8a = "DELETE FROM f_tree_users";
$sql8a = "INSERT INTO user_has_users_calendar (uid,be_users_id,color,hidden) VALUES (:uid,:userid,:color,:hidden); UPDATE user_has_users_calendar SET color=:col2, hidden=:hid2 WHERE uid = :uid AND be_users_id = :be_users_id";
$sql8 = "SELECT /*+ LEADING(U) INDEX(U USER_ID_TYPE_SURNAME) */ * FROM `aaa` WHERE (  c_workflow LIKE '%Pard%' OR `d1`.`name` LIKE '%Pard%' OR `df`.`name` LIKE '%Pard%' OR c_titles LIKE '%Pard%' OR firstname LIKE '%Pard%' OR surname LIKE '%Pard%' OR c_referee LIKE '%Pard%' OR c_nickname LIKE '%Pard%' OR c_byear LIKE '%Pard%' OR c_education LIKE '%Pard%' OR `u`.`city` LIKE '%Pard%' OR `u`.`description` LIKE '%Pard%' OR u.uid IN (SELECT cc15.user_address_id FROM contacts cc15 WHERE cc15.user_id = '520' AND cc15.value LIKE '%Pard%' ))";
$sql7 = "SELECT SQL_CALC_FOUND_ROWS  `d`.`uid`, d.`name`, d.`value`,d.`weight`, d.`class`, IFNULL(d.`OverlayHidden`,0) `OverlayHidden`, reldefs FROM definitions_cache d WHERE  upname = 520 AND OverlayHidden = 0 AND class != 'classes' AND TRIM(name) != '' ORDER BY name asc";
$sql7 = "UPDATE F_EVENTS SET `user_id` = :user_id, `start` = :start, `end` = :end, `rem` = :rem, `loc` = :loc, `cid` = :cid, `rrule_id` = :rrule_id, `ad` = :ad, `address_id` = :address_id, `activity_id` = :activity_id, `activity_type_id` = :activity_type_id, `source_id` = :source_id, `nextdate` = :nextdate, `plans_success` = :plans_success, `created` = :created, `updated` = :updated, `is_editable` = :is_editable, `title` = :title WHERE uid = :uid";
$sql7 = "INSERT /*+ ignore_row_on_dupkey_index(RECORDS_METADATA,PRIMARY_64) */ INTO records_metadata 
                     (domain, id, version, data) 
                     VALUES (:domain, :id, :version, :data) ";
$sql7 = "INSERT INTO user_addresses_has_tags (user_addresses_id,tags_id) /***/ SELECT u.uid, t.uid FROM user_addresses u, tags t
                WHERE t.uid IN(:EXTRACTED0) AND u.type IS NULL AND  u.uid IN (select `user_addresses_id` FROM `user_contacts_selection` WHERE `be_users_id` = 520 )  AND (  (u.user_id) = :user_id  OR ( (1=1)  AND u.uid IN (
                    SELECT v.user_addresses_id FROM user_addresses_visibility v WHERE v.be_users_id = :visibility_owner AND access_level = 10 AND (valid_to IS NULL OR valid_to > NOW())
                )))";                    
$sql7 = "INSERT INTO records_metadata 
                     (domain, id, version, data) 
                     VALUES (:domain, :id, :version, :data)
                     ON DUPLICATE KEY UPDATE SET id = :id";
/*
$sql7 = "SELECT `uid`, CONCAT(name, ' (', IFNULL(Partners_id, 'BT'), ')') as `name` FROM be_users WHERE Confirmed > 0 AND `uid` IN (
            SELECT `owner_id` FROM `user_addresses` WHERE type IS NULL AND `user_id` IN ($usersIds) AND `owner_id` NOT IN ($ownerIds)
        )
        UNION
        SELECT `uid`, CONCAT(name, ' (', IFNULL(Partners_id, 'BT'), ')') as name FROM be_users WHERE Confirmed > 0 AND `uid` IN(
            SELECT `owner_id` FROM `user_addresses` WHERE type IS NULL AND `owner_id` NOT IN ($ownerIds) AND uid IN (
                SELECT `user_addresses_id` FROM `user_addresses_visibility` WHERE be_users_id IN ($usersIds)
            )
        ) ORDER BY `name` DESC";
*/
$sql7 = "UPDATE `user_addresses` SET consent_approval_to = (SELECT MAX(valid_to) FROM documents WHERE user_addresses_id = :uid AND document_type IN (:EXTRACTED0)) WHERE uid = :uid";
         echo $o->process($sql7);
         echo "\n".microtime(true);
         echo "\n runtime: ".(microtime(true) - $start)."\n";
    }
}

//$translator = new OracleSQLTranslator(false);
//$translator->process("SELECT q.qid, question, gid FROM questions as q WHERE (select count(*) from answers as a where a.qid=q.qid and scale_id=0)=0 and sid=11929 AND type IN ('F', 'H', 'W', 'Z', '1') and q.parent_qid=0");
//$translator->process("SELECT *, (SELECT a from xyz WHERE b>1) haha, (SELECT *, b from zks,abc WHERE d=1) hoho FROM blubb d, blibb c WHERE d.col = c.col");
