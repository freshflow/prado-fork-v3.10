<?php

/**
 * Interface for Validation\Message
 */
interface ValidationMessageInterface
{
    /**
     * Sets message type
     * @param $type
     * @return ValidationMessage
     */
    public function setType($type);

	/**
     * Returns message type
     */
	public function getType();

    /**
     * Sets verbose message
     * @param string $message
     * @return ValidationMessage
     */
	public function setMessage($message);

	/**
     * Returns verbose message
     *
     * @return string
     */
	public function getMessage();

    /**
     * Sets field name related to message
     * @param $field
     * @return ValidationMessage
     */
	public function setField($field);

	/**
     * Returns field name related to message
     *
     * @return string
     */
	public function getField();

	/**
     * Magic __toString method returns verbose message
     */
	public function __toString();

    /**
     * Magic __set_state helps to recover messages from serialization
     * @param array $message
     * @return ValidationMessageInterface
     */
	public static function __set_state(array $message);
}