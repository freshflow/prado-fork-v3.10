<?php

class PresenceOf extends Validator
{

    /**
     * Executes the validation
     * @param Validation $validation
     * @param $field
     * @return bool
     * @throws Exception
     */
    public function validate(Validation $validation, $field)
    {
        $value = $validation->getValue($field);
        if (empty($value)) {
            $label = $this->prepareLabel($validation, $field);
            $message = $this->prepareMessage($validation, $field, 'PresenceOf');
            $code = $this->prepareCode($field);

            $replacePairs = [':field' => $label];

            $validation->appendMessage(
                new ValidationMessage(
                    strtr($message, $replacePairs),
                    $field,
                    'PresenceOf',
                    $code
                )
            );

            return false;
        }
        return true;
    }
}