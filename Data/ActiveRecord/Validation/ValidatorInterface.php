<?php

interface ValidatorInterface
{
    /**
     * Checks if an option is defined
     * @param $key
     * @return bool
     */
    public function hasOption($key);

    /**
     * Returns an option in the validator's options
     * Returns null if the option hasn't set
     * @param $key
     * @param null $defaultValue
     * @return mixed
     */
    public function getOption($key, $defaultValue = null);

    /**
     * Executes the validation
     * @param Validation $validation
     * @param $attribute
     * @return bool
     */
    public function validate(Validation $validation, $attribute);
}