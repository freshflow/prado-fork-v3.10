<?php  


class FSqlTransform extends TComponent
{
    /** @var TDbCommand */
	protected $_cmd;
	public static $cache = [];

    /**
     * @param TDbCommand $cmd
     * @return FMysqlSqlTransform|FOracleSqlTransform
     */
	public static function factory($cmd) {
		//$dn = $cmd->getConnection()->getDriverName();
		if (stripos($cmd->getConnection()->getConnectionString(),'oci') !== false) {
			return new FOracleSqlTransform($cmd);
		}
		
		return new FMysqlSqlTransform($cmd);
	}

    /**
     * FSqlTransform constructor.
     * @param TDbCommand $cmd
     */
	public function __construct($cmd)
	{
		$this->_cmd = $cmd;
	}
	
	public function doTransform() {}

    public static function bracketize($a) {
        return '(' . $a . ')';
    }

    public function extractLists()
	{
		$sql = $this->_cmd->getText();
//		$found = preg_match_all('/ IN *\(([0-9,]*[0-9]|(?:\'[0-9]+\',)*\'[0-9]\'|\((?:\'[0-9]+\',)*\'[0-9]\'\)|\([0-9,]*[0-9]\))\)/im',$sql,$matches,PREG_PATTERN_ORDER);
		$found = preg_match_all('/ IN *\(((?:\(\'[0-9]+\'\),? ?)+|(?:\'[0-9]+\',? ?)+|(?:[0-9]+,? ?)+)\)/im',$sql,$matches,PREG_PATTERN_ORDER);
		if ($found === 0) return;
		
		$from = array_unique($matches[1]);
		//sort by length of the value
		usort($from, function($a, $b) {
			return strlen($b) - strlen($a);
		});
		$to = [];
		$unEsc = &$this->_cmd->_unescapedBindings;
		foreach($from as $k => $m)
		{
			$name = ':EXTRACTED'.$k;
			$to[$k] = $name;
			$unEsc[$name] = (object) [ 'name'=>$name, 'param'=>$m ];
			//var_dump();
		}

        $from = array_map([$this, 'bracketize'], $from);
        $to = array_map([$this, 'bracketize'], $to);

        $sql = str_replace($from,$to,$sql);
		$this->_cmd->setText($sql);
	}
	
}

class FMysqlSqlTransform extends FSqlTransform
{
	
	private $conn;

	public function __construct($cmd)
	{
		parent::__construct($cmd);
		$this->conn = new TDbConnection('oci:dbname=vps77.nlooud.com;charset=UTF8','system','Lokomotiva606');
	}

	public function doTransform()
	{
		$this->extractLists();

        $logme = (Prado::getApp()->getMode() == TApplicationMode::Debug);

        if($logme)
        {
            $f = fopen('./db.log','a');
            $g = fopen('./dbErr.log','a');
        }


		if (preg_match('/SHOW|CONSTRAINT/im',$this->_cmd->getText())) {
            return;
        }

		$oSql = $this->_cmd->getText();
		$key = md5($oSql);
		if (isset(self::$cache[$key])) {
		    return;
        }

		if($logme) {
            fwrite($f, $oSql . "\n");
        }

		try {

			try {

				$ocmd = new TDbCommand($this->conn,$oSql);
				foreach($this->_cmd->getParamBindings() as $k=>$v)
					$ocmd->_paramBindings[$k] = clone $v;
				foreach($this->_cmd->getValueBindings() as $k=>$v)
					$ocmd->_valueBindings[$k] =  clone $v;
				foreach($this->_cmd->getUnescapedBindings() as $k=>$v)
					$ocmd->_unescapedBindings[$k] =  clone $v;
				
				$ora = FSqlTransform::factory($ocmd);

				$ora->doTransform();
				
				$ora->_cmd->bindUnescapedNow();				
				
				$cSql = str_replace("\n"," ",$ora->_cmd->getText());
				$params = [
					'sql'    => $cSql,
					'values' => $ora->_cmd->_valueBindings,
					'params' => $ora->_cmd->_paramBindings,											
				];							
				
				$p = base64_encode(json_encode($params));
				
				$postdata = http_build_query(
					array(
					    'p' => $p,					    
					)
				);
				
				$opts = array('http' =>
					array(
					    'method'  => 'POST',
					    'header'  => 'Content-type: application/x-www-form-urlencoded',
					    'content' => $postdata
					)
				);
				 
				$context  = stream_context_create($opts);

				$ret = file_get_contents('https://ao.freshflow.cz/oci.php',false, $context);

				if (stripos($ret,'error') !== false) {
					if($logme)
                    {
                        fwrite($g,$oSql."\n");
                        fwrite($g,$cSql."\n");
                        fwrite($g,json_encode($params['values'])."\n");
                        fwrite($g,json_encode($params['params'])."\n");
                        fwrite($g,json_encode($ocmd->_unescapedBindings)."\n");
                        fwrite($g,$ret."\n\n");
                    }

				}


			}
			catch(Exception $ee)
			{
				$cSql = "REMOTE FAILED... ".get_class($ee);
				if($logme)
                {
                    fwrite($g,$oSql."\n");
                    fwrite($g,$cSql."\n\n");
                }


			}
		}
		catch (Exception $e)
		{
			if($logme) {
                fwrite($g, $oSql . "\n\n");
            }

			$cSql = "CANNOT TRANSLATE...";
		}
		self::$cache[$key] = $cSql;

		if($logme) {
            fwrite($f, "ORACLE: " . $cSql . "\n");
        }

		if($logme)
        {
            foreach($this->_cmd->getUnescapedBindings() as $v)
            {
                fwrite($f,"   ".$v->name.' => '.$v->param."\n");
            }
            foreach($this->_cmd->getValueBindings() as $v)
            {
                fwrite($f,"   ".$v->name.' -> '.$v->param."\n");
            }
            foreach($this->_cmd->getParamBindings() as $v)
            {
                fwrite($f,"   ".$v->name.' -> '.$v->param."\n");
            }
            fclose($f);
            fclose($g);
        }
	}
}

class FOracleSqlTransform extends FSqlTransform
{
	const ORACLE_NO_TOUCH = '/* ORACLE */';
	const ORACLE_WITH_PARAMS = '/* ORACLE_PARAMS */';
	
	public static $transformationArray =  array( 
		'uid' => "UID",
		"level" => "LEVEL",
		"start" => "START",
		"end" => "END",
		"version" => "VERSION",
		"size" => "SIZE",
		"readonly" => "READONLY",
        "comment" => "COMMENT"
		);

	public static $dateFormats = [
		'YYYY-MM-DD HH24:MI:SS', 'YYYY-MM-DD HH24:MI', 'YYYY-MM-DD', 'YYYY-MM' 	
	];

	public static $dFormats = [
		'%x-%v'=>'YYYY-IW',
		'%xW%v'=>'YYYY"W"IW',
		'%Y-%m-%d' => 'YYYY-MM-DD',
		'%Y'=>'YYYY',
		'%m' => 'MM',
		'%d' => 'DD',
		'%H' => 'HH24',
		'%i' => 'MI',
		'%s' => 'SS',
		'%M' => 'MONTH'
 	];

	public static $functions =  array( 
		'now' => "SYSDATE",
		"curdate" => "TRUNC(SYSDATE)",
		"curtime" => "SYSDATE",
		"date_add" => "DATEADD",
		"date_format" => "TO_CHARDATE",
		"date_sub" => "DATESUB",
		"adddate" => "DATEADD",
		"subdate" => "DATESUB",
		"ifnull" => "NVL",
		"group_concat" => "LISTAGG",
		"concat" => "CONCAT",
		"concat_ws" => "CONCATWS",
		"if" => "CASE",
		"month" => "EXTRACT_M",
		"year" => "EXTRACT_Y",
		"date" => "TRUNC",
		"trim" => "TRIM_E",
		//"regexp" => "REGEXP_LIKE",
		"exists" => "EXISTS"
 		);

	
	public function doTransform()
	{
        static $qcache = [];
        $logme = (Prado::getApp()->getMode() == TApplicationMode::Debug);

        if ($logme) {
            $f = fopen('protected/runtime/db.log', 'a');
        }

        if (stripos($this->_cmd->getText(), self::ORACLE_NO_TOUCH) !== false) {
            if ($logme) {
                fwrite($f, $this->_cmd->getText() . "\n");
            }
            $sql = str_replace(self::ORACLE_NO_TOUCH, '', $this->_cmd->getText());
            $this->_cmd->setText($sql);
            return;
        }
		
		$this->extractLists();
		
		$this->escapeParams();
		$this->prepareDates();
		$this->prepareDateFormats();
		
		$returning = $this->cutReturning();
		$limit = $this->transLimit();
		
		$sql = $this->_cmd->getText();

		$key = __CLASS__ . ':SQL1:' . md5($sql);
		/** @var RedisCache $cache */
		$cache = Prado::getApplication()->getModule('RedisCache');

        $cSql = (isset($qcache[$key])) ? $qcache[$key] : $cache->read($key);
        if (!$cSql || Prado::getApp()->getMode() === 'Debug') {
            $ora = new PHPSQLParser\OracleTranslator($this->_cmd->getConnection()->getSchema());
            $cSql = $ora->process($this->_cmd->getText());
            $tags = [Cache::TAGS => array(Cache::RELEASE)];
            $cache->write($key, $cSql, $tags);
            $qcache[$key] = $cSql;

            if ($logme) {
                fwrite($f, $this->_cmd->getText() . "$returning\n");
                fwrite($f, TVarDumper::dump($this->_cmd->_paramBindings) . "\n");
                fwrite($f, TVarDumper::dump($this->_cmd->_valueBindings) . "\n");
                fwrite($f, TVarDumper::dump($this->_cmd->_unescapedBindings) . "\n");
                fwrite($f, $cSql . "\n");
            }

        } elseif ($logme) {
            fwrite($f, TVarDumper::dump($this->_cmd->_paramBindings) . "\n");
            fwrite($f, TVarDumper::dump($this->_cmd->_valueBindings) . "\n");
            fwrite($f, TVarDumper::dump($this->_cmd->_unescapedBindings) . "\n");
            fwrite($f, $cSql . "\n");
        }
		if ($logme) {
            fclose($f);
        }
		//$this->_cmd->setText($cSql.$limit.$returning);
		$this->_cmd->setText($cSql.$returning);
	}

    /**
     * @return string
     */
	public function transLimit() {
		$sql = $this->_cmd->getText();
		if (preg_match('/LIMIT *([0-9]+) *, *([0-9]*) *$/i',$sql,$m)) {
			
			$sql = str_replace($m[0],'',$sql);
			$this->_cmd->getText($sql);
			$offset = $m[2];
			$limit = $m[1];
			
			if (!$offset)
				return ' FETCH FIRST '.$limit.' ROWS ONLY ';
			else
				return ' OFFSET '.$offset.' ROWS FETCH NEXT '.$limit.' ROWS ONLY ';			
		}
		return '';
	}

    /**
     * @return bool|string
     */
	public function cutReturning()
	{
		$sql = $this->_cmd->getText();
		$pos = strrpos($sql,' RETURNING ');
		if ($pos === false) {
		    return '';
        }
		
		 $ret = substr($sql,$pos);
		 $this->_cmd->setText(substr($sql,0,$pos));
		 
		 return $ret;
	}

    /**
     * @param $v
     * @return bool
     */
	public function getIsReserved($v)
	{
		$v = trim($v,':');
		return isset(self::$transformationArray[strtolower($v)]);
	}

    /**
     *
     */
	public function escapeParams()
	{
		foreach($this->_cmd->_paramBindings as $k => &$v)
		{
			if ($this->getIsReserved($k)) {
				$v->name = ':Or_'.trim($k,':');
			}
			if (is_numeric($v->name)) {
				$v->name;
			}
		}
		unset($v);
		
		foreach($this->_cmd->_valueBindings as $k => &$v)
		{
			if ($this->getIsReserved($k)) {
				$v->name = ':Or_'.trim($k,':');
			}
		}
        unset($v);
	}

    /**
     * @param $d
     * @return null|string
     */
	protected function getIsDateTime($d) {
	    if (!is_string($d)) {
	        return null;
        }
		if ( \DateTime::createFromFormat('Y-m-d H:i:s',$d) ) {
	        return '_asdate0';
        }
		if ( \DateTime::createFromFormat('Y-m-d H:i',$d) ) {
	        return '_asdate1';
        }
		if ( \DateTime::createFromFormat('Y-m-d',$d) ) {
	        return '_asdate2';
        }
		if ( \DateTime::createFromFormat('Y-m',$d) ) {
	        return '_asdate3';
        }
	}

    /**
     *
     */
	public function prepareDates()
	{
		
		$from = []; $to = [];
		foreach($this->_cmd->getParamBindings() as $k => &$v)
		{
			if ($suffix = $this->getIsDateTime($v->param)) {
				$v->name = $k.$suffix;
				$from[] = $k;
				$to[] = $v->name;
			}
		}
		unset($v);
		
		foreach($this->_cmd->getValueBindings() as $k => &$v)
		{
			if ($suffix = $this->getIsDateTime($v->param)) {
				$v->name = $k.$suffix;
				$from[] = $k;
				$to[] = $v->name;
			}
		}
        unset($v);

		//$sql = str_replace($from,$to,$this->_cmd->getText());
        $sql = $this->_cmd->getText();
		foreach ($from as $k => $_from) {
            $sql = preg_replace('/' . $_from . "\b/", $to[$k], $sql);
        }
        $this->_cmd->setText($sql);
	}

    /**
     *
     */
	public function prepareDateFormats()
	{		
		foreach($this->_cmd->getValueBindings() as $k => &$v)
		{
			$ary = array_slice(self::$dFormats,0,3);
			foreach($ary as $f => $t) {
				if ($f === $v->param) {
					$v->param = $t;					
				}
			}
		}
	}
}

